import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_bloc.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_event.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_state.dart';
import 'package:flutter_bloc_api/bloc/main_bloc.dart';
import 'package:flutter_bloc_api/model/Move.dart';

class Move extends StatefulWidget {
  @override
  _MoveState createState() => _MoveState();
}

class _MoveState extends State<Move> {
  TextEditingController indexPage = TextEditingController();
  String prefixImgUrl = 'https://image.tmdb.org/t/p/w500';

  List<Movie> moveModel = [];

  @override
  void initState() {
    _fetchPosts(1);
    super.initState();
  }

  _fetchPosts(int page) async {
    BlocProvider.of<PostBloc>(context).add(DoFetchEvent(page));
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(indexPage);
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: BlocConsumer<PostBloc, PostStates>(
          builder: (context, state) {
            return Container(
                child: Column(
              children: [
                Container(
                    color: Colors.white,
                    child: Column(
                  children: [
                    TextField(
                      controller: indexPage,
                    ),
                    TextButton(
                        onPressed: () {
                          _fetchPosts(int.tryParse(indexPage.text));
                        },
                        child: Text('xem'))
                  ],
                )),
                Expanded(child: _buildUI(context))
              ],
            ));
          },
          listener: (ctx, state) {
            if (state is LoadingState) {
              return Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height,
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              );
            } else if (state is FetchSuccess) {
              moveModel = state.posts.results;
            }
          },
        ),
      ),
    );
  }

  Widget _buildUI(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      itemCount: moveModel.length,
      itemBuilder: (context, index) {
        final item = moveModel[index];
        return ListTile(
          tileColor: Color(0xff222222),
            title: ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  flex: 2,
                  child: Material(
                    elevation: 10,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    child: Container(
                      height: 100,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.red
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                          child: Image.network(prefixImgUrl + item.posterPath, fit: BoxFit.cover,)),
                    ),
                  ),
                ),
                Expanded(
                  flex: 4,
                    child: Container(
                      height: 80,
                      color: Colors.white,
                    ))
              ],
            ),
          ),
        ));
      },
    );
  }
}
