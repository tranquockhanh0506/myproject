import 'dart:core';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_bloc.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_event.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_state.dart';
import 'package:flutter_bloc_api/model/Move.dart';
import 'package:google_fonts/google_fonts.dart';
class Movie1 extends StatefulWidget {
  const Movie1({Key key}) : super(key: key);

  @override
  _Movie1State createState() => _Movie1State();
}

class _Movie1State extends State<Movie1> {
  String prefixImgUrl = 'https://image.tmdb.org/t/p/w500';
List arrcategori =[
  'Action', 'Crime' , 'conedy', 'Draw'
];
  List<Movie> moveModel = [];
  var fontweith ;
  var fontweith1 ;
  var fontweith2 ;
  @override
  void initState() {
    _fetchPosts(1);
    super.initState();
  }

  _fetchPosts(int page) async {
    BlocProvider.of<PostBloc>(context).add(DoFetchEvent(page));
  }

  @override
  void dispose() {
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.menu_outlined),
        actions: [
          Icon(Icons.search)
        ],
      ),
      body: Container(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(onPressed: (){
                  setState(() {
                    fontweith= FontWeight.bold;
                    fontweith1 = FontWeight.normal;
                  });
                }, child: Text('In Theater', style: GoogleFonts.limelight(textStyle: TextStyle(color: Colors.black,fontSize: 20 , fontWeight: FontWeight.normal),),)),
                TextButton(onPressed: (){
                  setState(() {
                    fontweith= FontWeight.bold;
                    fontweith1 = FontWeight.normal;
                  });
                }, child: Text('Box Office', style: GoogleFonts.limelight(textStyle: TextStyle(color: Colors.black,fontSize: 20),),)),
                TextButton(onPressed: (){
                  setState(() {
                    fontweith= FontWeight.bold;
                    fontweith1 = FontWeight.normal;
                  });
                }, child: Text('Comi', style: GoogleFonts.limelight(textStyle: TextStyle(color: Colors.black,fontSize: 20),),)),
              ],
            ),
        Container(
          height: 100,
          child: ListView.builder(
            itemCount: arrcategori.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context , index)
            {
            return Container(
              width: MediaQuery.of(context).size.width/3.5,
              child:  Center(child: Text('${arrcategori[index]}'
                , style: TextStyle(fontSize: 18, color: Colors.black),)),
            );
            },
          ),
        ),
        BlocConsumer<PostBloc ,PostStates>(
            builder: (context , state)
            {
              return Container(
                height: MediaQuery.of(context).size.height/1.6,
                child: ListView.builder(
                  itemCount: moveModel.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context , index)
                  {
                    final item = moveModel[index];
                    return Container(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        children: [
                          Expanded(
                            child: ClipRRect(
                              borderRadius:
                              BorderRadius.circular(40),
                              child: Image.network(prefixImgUrl + item.posterPath,fit:
                              BoxFit.cover,width: MediaQuery.of(context).size.width/1.5,
                                height: double.infinity,),
                            ),
                          ),
                          SizedBox(height: 15,),
                          Container(
                          width: MediaQuery.of(context).size.width/1.5,
                              child: Text(
                                '${item.title}',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 24),overflow: TextOverflow.ellipsis,textAlign: TextAlign.center,),),
                          SizedBox(height: 10,),
                          Row(
                            children: [
                              Icon(Icons.star,color: Colors.yellow,),
                              SizedBox(width: 5,),
                              Text('${item.voteAverage}')
                            ],
                          )
                        ],
                      ),
                    );
                  },
                ),
              );
            }, listener: (ctx, state)
        {
          if(state is LoadingState)
            {
              return Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height,
                alignment: Alignment.center,
                child: CircularProgressIndicator(),
              );
            }else if(state is FetchSuccess)
              {
                moveModel = state.posts.results;
              }
        }),


          ],
        ),
      ),

    );
  }
}
