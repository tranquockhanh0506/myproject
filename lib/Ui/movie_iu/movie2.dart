import 'package:flutter/material.dart';
class Movie2 extends StatefulWidget {
  const Movie2({Key key}) : super(key: key);

  @override
  _Movie2State createState() => _Movie2State();
}

class _Movie2State extends State<Movie2> {
  @override
  Widget build(BuildContext context) {
    double heights = MediaQuery.of(context).size.height/3;
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
            child:  Container(
              height:heights ,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage("https://images.unsplash.com/photo-1601314167099-232775b3d6fd?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1122&q=80"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Positioned(
            top: 210.0,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0)),
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(16.0),
                    margin: EdgeInsets.only(left: 50,right: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                  Column(
                    children: [
                      Icon(Icons.star,color: Colors.yellowAccent,),
                      Text('8.2')
                    ],
                  ),
                  Column(
                    children: [
                      Icon(Icons.star_border_outlined,),
                      Text('Rate This')
                    ],
                  ),
                  Column(
                    children: [
                      Icon(Icons.event,color:Colors.green,),
                      Text('Metstasscore')
                    ],
                  ),
                      ],
                    ),

                  ),
                  Container(
                    padding: EdgeInsets.all(10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Food v Ferrari',style: TextStyle(fontSize: 30.0,fontWeight: FontWeight.bold),),
                                SizedBox(height: 5,),
                                Container(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('2019'),
                                      Text('pc 13'),
                                      Text('32tsf'),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(child: Container(
                          alignment: Alignment.topRight,
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.pinkAccent,
                                borderRadius: BorderRadius.circular(15)
                              ),

                                child: IconButton(onPressed: (){}, icon: Icon(Icons.add)))))
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
      // Stack(
      //   children: [
      //
      //     Positioned(
      //          bottom: 1,
      //         child: Container(
      //
      //           height: 150,
      //       color: Colors.red,
      //     ))
      //   ],
      // ),
    );
  }
}
