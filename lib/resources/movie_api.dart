import 'dart:async';
import 'package:flutter_bloc_api/model/Move.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class MovieApi {
  Future<MovieResponse> fetchAlbum(int page) async {
    final response =
    await http.get(Uri.parse('http://api.themoviedb.org/3/movie/popular?api_key=e7631ffcb8e766993e5ec0c1f4245f93&page=$page'));
    if (response.statusCode == 200) {
      return MovieResponse.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load album');
    }
  }
}