import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_state.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_event.dart';
import 'package:flutter_bloc_api/resources/movie_api.dart';

class PostBloc extends Bloc<PostEvents, PostStates> {
  MovieApi repo = MovieApi();
  PostBloc(PostStates initialState) : super(initialState);
  @override
  Stream<PostStates> mapEventToState(PostEvents event) async* {
    if (event is DoFetchEvent) {
      yield LoadingState();
      try {
        final response = await repo.fetchAlbum(event.pageNum);
        yield FetchSuccess(posts: response);
      } catch (e) {
        yield ErorState(message: e.toString());
      }
    }
  }
}
