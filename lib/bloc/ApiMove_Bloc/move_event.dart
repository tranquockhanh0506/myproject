import 'package:equatable/equatable.dart';
class PostEvents extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props =>[];

}
class DoFetchEvent extends PostEvents{
  final int pageNum;
  DoFetchEvent(this.pageNum);
  @override
  List<Object> get props => [pageNum];

}
