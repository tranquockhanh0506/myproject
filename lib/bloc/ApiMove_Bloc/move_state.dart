import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_api/model/Move.dart';

class PostStates extends Equatable{
  @override
  // TODO: implement props
  List<Object> get props => [];

}
class InitialState extends PostStates{}
class LoadingState extends PostStates{}
class FetchSuccess extends PostStates{
  MovieResponse posts ;
  FetchSuccess({this.posts});
  @override
  // TODO: implement props
  List<Object> get props => [posts];
}
class ErorState extends PostStates{
  String message;
  ErorState({this.message});
  @override
  // TODO: implement props
  List<Object> get props => [message];
}