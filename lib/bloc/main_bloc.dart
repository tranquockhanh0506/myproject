import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_bloc.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_state.dart';

class MainBloc {

  static List<BlocProvider> allBlocs() {
    return [
      BlocProvider<PostBloc>(
        create: (BuildContext context) => PostBloc(InitialState()),
      ),
    ];
  }

}