import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_api/Ui/move/move_screen.dart';
import 'package:flutter_bloc_api/bloc/ApiMove_Bloc/move_bloc.dart';
import 'package:flutter_bloc_api/bloc/main_bloc.dart';
import 'package:flutter_bloc_api/resources/repository.dart';

import 'Ui/movie_iu/movie1.dart';
import 'Ui/movie_iu/movie2.dart';
import 'bloc/ApiMove_Bloc/move_state.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: MainBloc.allBlocs(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Movie2(),
      ),
    );
  }
}
